#include <jni.h>
#include <string>

std::string SERVER_URL          = "http://demo.redtvlive.com/oxoo/v13/rest-api/";
std::string API_KEY             = "spagreen";
std::string PURCHASE_CODE       = "xxxxxxxxxxxxxxx";
std::string ONESIGNAL_APP_ID    = "xxxxxxxxxxxxxxx";


//WARNING: ==>> Don't change anything below.
extern "C" JNIEXPORT jstring JNICALL
Java_com_code_files_AppConfig_getApiServerUrl(
        JNIEnv* env,
        jclass clazz) {
    return env->NewStringUTF(SERVER_URL.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_code_files_AppConfig_getApiKey(
        JNIEnv* env,
jclass clazz) {
return env->NewStringUTF(API_KEY.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_code_files_AppConfig_getPurchaseCode(
        JNIEnv* env,
        jclass clazz) {
    return env->NewStringUTF(PURCHASE_CODE.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_code_files_AppConfig_getOneSignalAppID(
        JNIEnv* env,
        jclass clazz) {
    return env->NewStringUTF(ONESIGNAL_APP_ID.c_str());
}